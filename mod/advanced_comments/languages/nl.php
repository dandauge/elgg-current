<?php
/**
 * This file was created by Translation Editor v7.0-beta
 * On 2018-12-14 13:25
 */

return array (
  'advanced_comments:settings:auto_load:help' => 'laad automatisch de volgende serie reacties in als de gebruiker het einde van de pagina bereikt',
  'advanced_comments:settings:user_preference' => 'Mogen gebruikers de reactie instellingen aanpassen',
  'advanced_comments:comment:logged_out' => 'Reageren is alleen toegestaan voor aangemelde gebruikers',
  'advanced_comments:settings:show_login_form' => 'Toon aanmeldformulier voor afgemelde gebruikers onder de reacties',
  'advanced_comments' => 'Geadvanceerde reacties',
  'advanced_comments:header:order' => 'Reactie volgorde',
  'advanced_comments:header:order:asc' => 'Oudste eerst',
  'advanced_comments:header:order:desc' => 'Nieuwste eerst',
  'advanced_comments:header:limit' => 'Aantal',
  'advanced_comments:header:auto_load' => 'Automatisch laden',
);
