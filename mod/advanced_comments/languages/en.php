<?php

return [
	'advanced_comments' => "Advanced comments",
	
	'advanced_comments:header:order' => "Comments order",
	'advanced_comments:header:order:asc' => "Oldest first",
	'advanced_comments:header:order:desc' => "Newest first",
	
	'advanced_comments:header:limit' => "Limit",
	'advanced_comments:header:auto_load' => "Auto Load",
	
	'advanced_comments:comment:logged_out' => "Commenting only available for logged in users",
	
	'advanced_comments:settings:auto_load:help' => "Automaticly load the next batch of comments when the user reaches the end of the page",
	'advanced_comments:settings:user_preference' => "Are users allowed to change the comment settings",
	
	'advanced_comments:settings:show_login_form' => "Show login form for logged out users below comments",
	
];
