<?php

return [
	'settings' => [
		'crontrigger_minute' => 0,
		'crontrigger_fiveminute' => 0,
		'crontrigger_fifteenmin' => 0,
		'crontrigger_halfhour' => 0,
		'crontrigger_hour' => 0,
		'crontrigger_day' => 0,
		'crontrigger_week' => 0,
		'crontrigger_month' => 0,
		'crontrigger_year' => 0,
	],
];
