<?php

return array (
  'freichat:settings:token' => 'Jeton',
  'freichat:settings:token:help' => 'Un jeton utilisé pour une communication sécurisée. Veuillez ne pas modifier ce point.',
  'freichat:settings:friends' => 'Voir seulement les contacts ?',
  'freichat:settings:friends:help' => 'Dans ce cas, seuls les utilisateurs qui sont en contact seront listés.',
);
