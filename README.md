Elgg Starter Project [![Build Status](https://travis-ci.org/Elgg/starter-project.svg?branch=master)](https://travis-ci.org/Elgg/starter-project)
====

En
This is a quickstart skeleton for building a site based on Elgg 3.x.

Fr
Voici une pile d'installation d'Elgg dans sa version courante 3.x.

## Installation

En
Follow the instructions for [installing Elgg with Composer](http://learn.elgg.org/en/stable/intro/install.html#overview). Otherwise, download the zip and unzip it to the folder of your choice.

Fr
Pour installer Elgg avec Composer, suivre les instructions [de la documentation officielle](http://learn.elgg.org/en/stable/intro/install.html#overview). Autrement, téléchargez le zip et décompressez-le à l'endroit de votre choix.

## Composants additionnels

En
In the /Mod directory a number of modules have been added in complement to those originally included. They have been selected because they are all followed by people who have been using the tool for a long time.

FR
Dans le répertoire /Mod ont été rajoutés un certain nombre de modules en plus de ceux fournis initialement. Ils ont été sélectionnés car ils sont tous suivis par des personnes présentent depuis longtemps sur l’outil (sauf le thème).

Liste des modules rajoutés :

- Account Removal
- Advanced Comments
- Advanced Notifications
- Blog tools
- CKEditor Extended
- Content Subscriptions
- Cookie banner
- Croncheck
- Crontrigger
- Custom css
- Customsize group image
- Discussions Tools
- Embed Extended
- Event Manager
- Elgg Update Services
- Favicon Override (modifié)
- FreiChat
- Group Tools
- Header Logo Changer (modifié)
- Http Blacklist
- HTML Email Handler
- iZAP Videos - revised edition by iionly
- Mmenu
- Newsletter
- oEmbed
- Profile Manager
- Poll
- Site announcements
- Tag tools
- Tidypics
- Target Blank
- Translation Editor
- Widget Manager
- The Wire Tools
- The Wire Websitecards (modifié)
- The Wire pro (modifié)
